include conf.mk

ifndef DEBUG
  DEBUG=yes
endif

ifndef VERBOSE
  VERBOSE=no
endif

ifeq ($(DEBUG),yes)
  FLAGS+=-g
  VERSION+=-debug
else
  WARNINGS+=error
  FLAGS+=-O2
endif

ifeq ($(VERBOSE),yes)
  AT=
else
  AT=@
endif

DFLAGS=$(addprefix -D, $(DEFINES)) \
       -DCOMPILETIME='$(shell date +"%Y-%m-%d %H:%M %z")' \
       -DVERSION='$(VERSION)'

WFLAGS=$(addprefix -W, $(WARNINGS))

FLAGS+=$(WFLAGS) --std=$(STD) -pedantic
FLAGS+=-pthread -I$(INCDIR) -I. -fPIC -fdiagnostics-color=always
FLAGS+=$(DFLAGS)

LFLAGS=

HFILES=$(addprefix $(INCDIR), $(addsuffix .h, $(FILES) $(HEADERS)))
CFILES=$(addprefix $(SRCDIR), $(addsuffix .c, $(FILES)))
OFILES=$(addprefix $(OBJDIR), $(addsuffix .o, $(FILES)))

ifeq ($(DEBUG), yes)
  ERRPIPE=2>&1 | tee -a errs.txt || (less -R errs.txt && /bin/false)
else
  ERRPIPE=
endif

DEPMATCH=^\S+\.o: $(SRCDIR)(\S+)\.c
DEPREPL=$(OBJDIR)\1\.o: $(SRCDIR)\1\.c
DEPCMD=sed -re "s/$(subst /,\\/,$(DEPMATCH))/$(subst /,\\/,$(DEPREPL))/" | sed -re "s/^test\.o/bin\/tests/g"

$(DEPFILE): $(CFILES) $(HFILES) test/test.c
	$(AT)$(CC) -MM $(FLAGS) $^ | $(DEPCMD) > $@
	$(AT)printf "Built dependencies\n"

$(OBJDIR)%.o: $(SRCDIR)%.c conf.mk
	$(AT)mkdir -p $(@D)
	$(AT)$(CC) -c $(FLAGS) $< -o $@ $(ERRPIPE)
	$(AT)printf "Built $@\n"

$(BINDIR)esta: $(OFILES) $(SRCDIR)esta.c
	$(AT)mkdir -p $(@D)
	$(AT)$(CC) $(FLAGS) $^ -o $@ $(LFLAGS) $(ERRPIPE)
	$(AT)printf "Built $@\n"

$(BINDIR)tests: $(OFILES) test/test.c
	$(AT)mkdir -p $(@D)
	$(AT)$(CC) $(FLAGS) $^ -o $@ $(LFLAGS) $(ERRPIPE)
	$(AT)printf "Built $@\n"

all: deps $(BINDIR)esta $(BINDIR)tests
	$(AT)if [ -s errs.txt ]; then cat errs.txt | less -R; rm errs.txt; fi

install: all
	$(AT)cp $(BINDIR)edil $(INSTDIR)edil
	$(AT)printf "Installed edil in $(INSTDIR)edil\n"

uninstall:
	$(AT)rm -f $(INSTDIR)/edil
	$(AT)printf "Uninstalled edil\n"

clean_err:
	$(AT)rm -f errs.txt

clean_bin:
	$(AT)rm -rf $(BINDIR)
	$(AT)printf "Removed $(BINDIR)\n"

clean_obj:
	$(AT)rm -rf $(OBJDIR)
	$(AT)printf "Removed $(OBJDIR)\n"

clean_dep:
	$(AT)rm -rf $(DEPDIR)
	$(AT)printf "Removed $(DEPDIR)\n"

clean_docs:
	$(AT)make -C doc clean
	$(AT)make -C layouts clean

docs: all
	$(AT)make -C doc all
	$(AT)make -C layouts all

deps: $(DEPFILE)
clean: clean_err clean_bin clean_obj clean_dep

.PHONEY=deps all clean_err clean_bin clean_obj clean_dep clean install uninstall test.o
.DEFAULT_GOAL=all

$(shell rm -f errs.txt)

ifeq (,$(findstring clean,$(MAKECMDGOALS)))
  ifeq (,$(findstring deps,$(MAKECMDGOALS)))
    $(info Including depenendencies ...)
    include $(DEPFILE)
  endif
endif
