FILES=vec val parser print table eval builtins run

SRCDIR=src/
INCDIR=inc/
OBJDIR=obj/
BINDIR=bin/
DEPFILE=deps.d

FLAGS=-Isnow/snow

INSTDIR=/usr/local/bin/

VERSION=0.2.0

WARNINGS=all extra no-unused-parameter no-switch missing-prototypes

STD=gnu11

DEFINES=

