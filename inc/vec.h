#if !defined(VEC_H)
# define VEC_H
# include <string.h>
# include "stddef.h"

typedef struct vec vec_s;
struct vec
{
    char *mem;
    size_t width, allocated, used;
};

void vec_init(vec_s *v, size_t width);

void vec_kill(vec_s *v);

void *vec_ins(vec_s *v, size_t ind, size_t n, void *mem);

int vec_del(vec_s *v, size_t ind, size_t n);

void *vec_get(vec_s *v, size_t ind);

size_t vec_len(vec_s *v);

void *vec_fmt(vec_s *v, char *fmt, ...);

#define VEC_FOREACH(vec, type, name) \
    type name;                       \
    for (size_t _ind = 0,            \
         _len = vec_len(vec);        \
         name = vec_get(vec, _ind),  \
         _ind < _len;                \
         ++_ind)

#define VEC_RFOREACH(vec, type, name) \
    type name;                        \
    for (size_t _ind = 0,             \
         _len = vec_len(vec);         \
         name = vec_get(vec, _len - _ind - 1), \
         _ind < _len;                 \
         ++_ind)

static inline void *vec_end(vec_s *v)
{
    return vec_get(v, vec_len(v) - 1);
}

static inline void *vec_add(vec_s *v, size_t n, void *mem)
{
    return vec_ins(v, vec_len(v), n, mem);
}

static inline void *vec_app(vec_s *v, void *mem)
{
    return vec_add(v, 1, mem);
}

static inline void *vec_cpy(vec_s *v, vec_s *oth)
{
    return vec_add(v, vec_len(oth), vec_get(oth, 0));
}

static inline void *vec_str(vec_s *v, char *str)
{
    return vec_add(v, strlen(str), str);
}


#endif
