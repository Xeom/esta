#if !defined(EVAL_H)
# define EVAL_H

typedef struct frame frame_s;

# include "val.h"
# include "table.h"

struct frame
{
    frame_s *parent;
    table_s  local;
    size_t   ind;
    val_s   *var;
    val_s   *rtn;
};

void eval_descend(frame_s **f, val_s *var);

val_s *eval_ascend(frame_s **f);

val_s *eval_varify(val_s *v, table_s *local, table_s *global);

val_s *eval_defaultify(val_s *v, table_s *local);

val_s *eval_r_varify(val_s *v, table_s *local, table_s *global);

val_s *eval_r_defaultify(val_s *v, table_s *local);

val_s *eval_callify(val_s *v, frame_s *f, table_s *globals);

val_s *eval_r_callify(val_s *v, frame_s *f, table_s *globals);

val_s *eval_line(val_s *v, frame_s *f, table_s *globals);

val_s *eval_deferred(val_s *v, frame_s *f, table_s *globals, table_s *locals);

int eval_set_str_names(table_s *scope, val_s *name, val_s *v);

#endif
