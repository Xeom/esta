#if !defined(TYPES_H)
# define TYPES_H
# include <stdbool.h>

typedef struct val val_s;

# include "vec.h"
# include "eval.h"
# include "builtins.h"
# include "table.h"

typedef enum
{
    TYPE_INT,
    TYPE_FLOAT,
    TYPE_BUILTIN,
    TYPE_LIST,
    TYPE_DEFERRED,
    TYPE_CALL,
    TYPE_STR,
    TYPE_NAME
} val_type_t;

#define TYPE_IS_VEC(type)     (type == TYPE_LIST || type == TYPE_DEFERRED || type == TYPE_CALL || type == TYPE_STR || type == TYPE_NAME)
#define TYPE_IS_CHARVEC(type) (type == TYPE_NAME || type == TYPE_STR)
#define TYPE_IS_VALVEC(type)  (type == TYPE_LIST || type == TYPE_DEFERRED || type == TYPE_CALL)

typedef val_s *(*builtin_funct_t)(val_s *, frame_s *, table_s *);

struct val
{
    int refs;
    val_type_t type;
    union {
        int i;
        double f;
        vec_s v;
        builtin_funct_t builtin;
    } val;
};

bool val_eq(val_s *a, val_s *b);

bool val_bool(val_s *val);

void val_deref(val_s *val);

void val_ref(val_s *val);

val_s *val_create(val_type_t type);

val_s *val_editable(val_s *val);

int val_ins(val_s *val, size_t ind, size_t n, val_s **new);

int val_app(val_s *val, val_s *new);

size_t val_len(val_s *val);

int val_incr(val_s *a, val_s *b);

int val_decr(val_s *a, val_s *b);

int val_mult(val_s *a, val_s *b);

#endif
