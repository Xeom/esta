#if !defined(PRINT_H)
# define PRINT_H
# include <stdio.h>
# include "val.h"

void print_valvec(val_s *v, vec_s *dst);

void print_stringval(val_s *v, vec_s *dst);

int print_val(val_s *v, vec_s *dst);

void print_val_to_file(val_s *v, FILE *f);

#endif
