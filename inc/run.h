#if !defined(RUN_H)
# define RUN_H
# include <stdio.h>
# include "table.h"
# include "val.h"
# include "parser.h"
# include "print.h"


val_s *parse_line(char *str);

val_s *parse_file(FILE *f);

void run_file(FILE *f, table_s *globals);

void run_line(char *str, table_s *globals);

#endif
