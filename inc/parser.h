#if !defined(PARSER_H)
# define PARSER_H
# include <stdbool.h>
# include "val.h"

typedef struct parser parser_s;

typedef enum
{
    PARSER_IDLE,
    PARSER_BABY_STRING,
    PARSER_STRING,
    PARSER_STRING_ESCAPE,
    PARSER_STRING_ESCAPE_HEX1,
    PARSER_STRING_ESCAPE_HEX2,
    PARSER_CALL,
    PARSER_DEFERRED,
    PARSER_LIST,
    PARSER_HEX,
    PARSER_ZERO,
    PARSER_NUMBER,
    PARSER_FLOAT,
    PARSER_NAME
} parser_state_t;

struct parser
{
      parser_state_t state;
      val_s *val;
      parser_s *parent;
};

void parser_init(parser_s *p, val_type_t type);

void parser_kill(parser_s *p);

int parser_str(parser_s **p, char *str);

int parser_chr(parser_s **p, int c);

int parser_process(parser_s **p, int c);


static inline bool parser_is_whitespace(char c)
{
    switch (c)
    {
    case '\t':   case '\n': case '\x0b':
    case '\x0c': case '\r': case ' ':
        return true;
    default:
        return false;
    }
}

static inline bool parser_is_number(char c)
{
    return (c >= '0' && c <= '9');
}

static inline bool parser_is_namestart(char c)
{
    if (c < 0x20) return false;
    if (c > 0x7e) return false;
    if (parser_is_whitespace(c)) return false;
    if (parser_is_number(c)) return false;

    switch (c)
    {
    case '.': case '[': case ']': case '(':
    case ')': case '{': case '}': case '\'':
    case '"':
        return false;
    default:
        return true;
    }
}

static inline bool parser_is_namemiddle(char c)
{
    if (c < 0x20) return false;
    if (c > 0x7e) return false;
    if (parser_is_whitespace(c)) return false;

    switch (c)
    {
    case '[': case ']': case '(':
    case ')': case '{': case '}':
        return false;
    default:
        return true;
    }
}

#endif
