#if !defined(TABLE_H)
# define TABLE_H
# include "vec.h"

typedef struct table       table_s;
typedef struct table_chain table_chain_s;

struct table_chain
{
    table_chain_s *next;
    size_t keylen;
    size_t vallen;
    char data[];
};

struct table
{
    size_t nitems;
    size_t nchains;
    vec_s chains;
};

#define TABLE_MIN_SIZE 8

#define TABLE_FOREACH(table, chain) \
    table_chain_s *chain = NULL;    \
    for (size_t _ind = 0;           \
         (chain = table_next(table, &_ind, chain)); )

void table_init(table_s *t);

void table_kill(table_s *t);

void *table_chain_key(table_chain_s *chain);

void *table_chain_val(table_chain_s *chain);

void *table_get(table_s *t, void *key, size_t keylen, size_t *vallen);

void table_set(table_s *t, void *key, size_t keylen, void *val, size_t vallen);

int  table_del(table_s *t, void *key, size_t keylen);

table_chain_s *table_next(table_s *t, size_t *ind, table_chain_s *chain);

#endif
