#define SNOW_ENABLED
#include "test/test.h"
#include "test/if.h"
#include "test/for.h"
#include "test/call.h"
#include "eval.h"
#include "parser.h"
#include "print.h"
#include <stdlib.h>

val_s *parse_str(char *str)
{
    parser_s  parser;
    parser_s *p;
    val_s *rtn;

    p = &parser;
    parser_init(p, TYPE_DEFERRED);

    parser_str(&p, str);
    rtn = parser.val;
    val_ref(rtn);

    parser_kill(p);

    return rtn;
}

val_s *run_str(char *str)
{
    table_s  globals;
    val_s   *rtn;
    frame_s *f = NULL;

    table_init(&globals);
    builtins_add(&globals);

    rtn = parse_str(str);
    rtn = eval_deferred(rtn, f, &globals, NULL);

    TABLE_FOREACH(&globals, pair)
    {
        val_s **v;
        v = table_chain_val(pair);
        val_deref(*v);
    }
    table_kill(&globals);

    return rtn;
}

char *val_to_str(val_s *v)
{
    vec_s chars;
    char *rtn;

    vec_init(&chars, sizeof(char));
    print_val(v, &chars);
    vec_app(&chars, NULL);

    rtn = malloc(vec_len(&chars));
    memcpy(rtn, vec_get(&chars, 0), vec_len(&chars));

    vec_kill(&chars);

    return rtn;
}

bool result_eq(char *a, char *b, char **ares, char **bres)
{
    val_s *aval, *bval;
    bool rtn;

    aval = run_str(a);
    bval = run_str(b);

    rtn = val_eq(aval, bval);

    *ares = val_to_str(aval);
    *bres = val_to_str(bval);

    val_deref(aval);
    val_deref(bval);

    return rtn;
}

snow_main();
