#if !defined(TEST_TEST_H)
# define TEST_TEST_H
# include "val.h"

# define assert_results_eq(a, b) \
    do {                         \
        char *astr, *bstr;       \
        defer(free(astr)); \
        defer(free(bstr)); \
        if (!result_eq(a, b, &astr, &bstr)) \
        { \
            fail("Expected evaluation of {" a "} and {" b \
                 "} to be equal, but they evaluated to " \
                 "\033[34m{\033[1m%s\033[0;34m}\033[0m and " \
                 "\033[34m{\033[1m%s\033[0;34m}\033[0m.", \
                 astr, bstr); \
        } \
    } while (0)

val_s *parse_str(char *str);

val_s *run_str(char *str);

char *val_to_str(val_s *v);

bool result_eq(char *a, char *b, char **ares, char **bres);

#endif
