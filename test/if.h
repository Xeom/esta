#if !defined(TEST_IF_H)
# define TEST_IF_H
# include "test/test.h"
# include "snow.h"

describe(if)
{
    it ("Works with integers")
    {
        assert_results_eq("(if 0 'a 'b)", "'b");
        assert_results_eq("(if 1 'a 'b)", "'a");
        assert_results_eq("(if 100 'a 'b)", "'a");
    }

    it ("Works with strings")
    {
        assert_results_eq("(if \"\" 'a 'b)", "'b");
        assert_results_eq("(if \"something\" 'a 'b)", "'a");
        assert_results_eq("(if 'x 'a 'b)", "'a");
    }
    it ("Works with one value")
    {
        assert_results_eq("(if 0 'a)", "");
        assert_results_eq("(if 1 'a)", "'a");
    }

    it ("Works in if-else configuration")
    {
        assert_results_eq("(if 1 'a  0 'b  0 'c)", "'a");
        assert_results_eq("(if 0 'a  1 'b  0 'c)", "'b");
        assert_results_eq("(if 0 'a  0 'b  1 'c)", "'c");
        assert_results_eq("(if 1 'a  1 'b  1 'c)", "'a");
        assert_results_eq("(if 0 'a  1 'b  1 'c)", "'b");
        assert_results_eq("(if 0 'a  0 'b  0 'c)", "");
        assert_results_eq("(if 0 'a  0 'b  0 'c 'd)", "'d");
    }

    it ("Works with deferred code")
    {
        assert_results_eq("(global 'x 1) (if x {(+ x x)} {\"x is too small\"})", "2");
        assert_results_eq("(global 'x 0) (if x {(+ x x)} {\"x is too small\"})", "\"x is too small\"");

        assert_results_eq("(global 'x 1) (if x {(global 'a 0)} {(global 'a 1)}) a", "0");
        assert_results_eq("(global 'x 0) (if x {(global 'a 0)} {(global 'a 1)}) a", "1");

        assert_results_eq("(global 'a 0) (if 0 {(global 'a 1)}) a", "0");
    }

    it ("Works with non-deferred code")
    {
        assert_results_eq("(global 'x 0) (if x [(global 'x 1)] 'hello) x", "1");
        assert_results_eq("(global 'x 1) (if x [(global 'x 1)] [(global 'x 0)]) x", "0");
    }

    it ("Works nested")
    {
        assert_results_eq("(if 0 (if 0 'a 'b) (if 0 'c 'd))", "'d");
        assert_results_eq("(if 0 (if 0 'a 'b) (if 1 'c 'd))", "'c");
        assert_results_eq("(if 1 (if 0 'a 'b) (if 0 'c 'd))", "'b");
        assert_results_eq("(if 1 (if 1 'a 'b) (if 0 'c 'd))", "'a");
    }

    it ("Works nested and deferred")
    {
        assert_results_eq("(if 0 {(if 0 'a 'b)} {(if 0 'c 'd)})", "'d");
        assert_results_eq("(if 0 {(if 0 'a 'b)} {(if 1 'c 'd)})", "'c");
        assert_results_eq("(if 1 {(if 0 'a 'b)} {(if 0 'c 'd)})", "'b");
        assert_results_eq("(if 1 {(if 1 'a 'b)} {(if 0 'c 'd)})", "'a");
    }
}

#endif
