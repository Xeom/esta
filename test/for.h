#if !defined(TEST_FOR_H)
# define TEST_FOR_H
# include "test/test.h"
# include "snow.h"

describe (for)
{
    it ("Can be used to increment array")
    {
        assert_results_eq("(set 'arr [10 20 30 40]) (for 'x arr {(+ x 10)})", "[20 30 40 50]");
        assert_results_eq("(for 'x [1 2 3 4] {(+ 1 x)})", "[2 3 4 5]");
        assert_results_eq("(for 'x ['dog 'cat] {(+ \"I have a \" x)})", "[\"I have a dog\" \"I have a cat\"]");
    }

    it ("Can be used on strings")
    {
        assert_results_eq(
            "(for 'x 'abc {(+ x \" is a letter\")})",
            "[\"a is a letter\" \"b is a letter\" \"c is a letter\"]"
        );
    }

    it ("Works nested")
    {
        assert_results_eq(
            "(for 'x 'ab {(for 'y 'ab {(for 'z 'ab {(+ x y z)} )} )} )",
            "[ [ [ 'aaa 'aab ] [ 'aba 'abb ] ] [ [ 'baa 'bab ] [ 'bba 'bbb ] ] ]"
        );

        assert_results_eq(
            "(global 'res [])"
            "(for 'x 'ab {"
                "(for 'y 'ab {"
                    "(for 'z 'ab {"
                        "(global 'res (+ res [(+ x y z)]))"
                    "})"
                "})"
            "})"
            "res",
            "[ 'aaa 'aab 'aba 'abb 'baa 'bab 'bba 'bbb ]"
        );
    }

    it ("Can build families of functions")
    {
        assert_results_eq(
            "(set 'functs (for 'x 'xyz {{(+ x y)}} ))"
            "(for 'f functs {(for 'y 'xyz f)})",
            "[['xx 'xy 'xz] ['yx 'yy 'yz] ['zx 'zy 'zz]]"
        );
    }

    it ("Executes in the correct order")
    {
        assert_results_eq("(for 'x ['a 'b 'c] {(global 'v x)}) v", "'c");
    }

    it ("Excludes null results")
    {
        assert_results_eq("(for 'x [1 0 2 0 3 0 4] {(if x x)})", "[1 2 3 4]");
    }

    it ("Calculates squares to 100")
    {
        assert_results_eq("(for 'x [1 2 3 4 5 6 7 8 9 10] {(* x x)})", "[1 4 9 16 25 36 49 64 81 100]");
    }
}

#endif
