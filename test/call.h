#if !defined(TEST_CALL_H)
# define TEST_CALL_H
# include "test/test.h"
# include "snow.h"

describe (call)
{
    it ("Can be used to sum a vector")
    {
        assert_results_eq("(call + [1 2 3 4 5 6])", "21");
    }

    it ("Can be used to multiply a vector")
    {
        assert_results_eq("(call * [1 2 3 4 5 6])", "720");
    }

    it ("Can call itself")
    {
        assert_results_eq("(call call [+ ['Hello \", world\"]])", "\"Hello, world\"");
    }
}

#endif
