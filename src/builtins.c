#include "val.h"
#include "table.h"
#include "eval.h"
#include "print.h"

#include "builtins.h"

struct builtin_pair {
    builtin_funct_t f;
    char *name;
};

static val_s *builtin_global(val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_set   (val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_plus  (val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_minus(val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_multiply(val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_if    (val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_for   (val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_default(val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_call   (val_s *call, frame_s *f, table_s *globals);
static val_s *builtin_print  (val_s *call, frame_s *f, table_s *globals);
struct builtin_pair builtins[] = {
    { builtin_set,    "set" },
    { builtin_plus,   "+" },
    { builtin_minus,   "-" },
    { builtin_multiply, "*" },
    { builtin_global, "global" },
    { builtin_if,     "if"     },
    { builtin_for,    "for"    },
    { builtin_default, "default" },
    { builtin_call,    "call"    },
    { builtin_print,   "print"   }
    // gset
    // gget
    // +
    // -
    // /
    // *
    // rep
    // for
    // if
    // null
};

void builtins_add(table_s *globals)
{
    size_t ind;
    for (ind = 0; ind < sizeof(builtins)/sizeof(builtins[0]); ++ind)
    {
        struct builtin_pair *pair;
        val_s *v;

        pair = &(builtins[ind]);

        v = val_create(TYPE_BUILTIN);
        v->val.builtin = pair->f;

        table_set(globals,
            pair->name, strlen(pair->name),
            &v,         sizeof(val_s *)
        );
    }
}

static val_s *builtin_if(val_s *call, frame_s *f, table_s *globals)
{
    size_t ind, nargs;
    vec_s *args;
    val_s *rtn;

    if (call->type != TYPE_CALL)
        return NULL;

    args = &(call->val.v);
    nargs = vec_len(args);
    if (nargs < 3)
        return NULL;

    for (ind = 1; ind < nargs - 1; ind += 2)
    {
        val_s **cond, **yes;
        cond = vec_get(args, ind);
        yes  = vec_get(args, ind + 1);

        if (val_bool(*cond))
        {
            rtn = eval_deferred(*yes, f, globals, NULL);
            *yes = NULL;
            return rtn;
        }
    }

    if ((nargs % 2) == 0)
    {
        val_s **no;
        no  = vec_get(args, nargs - 1);
        rtn = eval_deferred(*no, f, globals, NULL);
        *no = NULL;
        return rtn;
    }

    return NULL;
}

static val_s *builtin_for(val_s *call, frame_s *f, table_s *globals)
{
    size_t nargs;
    vec_s *args;
    val_s *rtn, **name, **items, **code;

    if (call->type != TYPE_CALL)
        return NULL;

    args = &(call->val.v);
    nargs = vec_len(args);

    if (nargs != 4)
        return NULL;

    name  = vec_get(args, 1);
    items = vec_get(args, 2);
    code  = vec_get(args, 3);

    if ((*name)->type != TYPE_STR)
        return NULL;

    rtn = val_create(TYPE_LIST);

    if (TYPE_IS_VALVEC((*items)->type))
    {
        table_s locals;
        table_init(&locals);

        VEC_FOREACH(&(*items)->val.v, val_s **, item)
        {
            val_s *result;
            eval_set_str_names(&locals, *name, *item);
            val_ref(*code);
            result = eval_deferred(*code, f, globals, &locals);
            if (result)
            {
                val_app(rtn, result);
                val_deref(result);
            }
        }

        eval_set_str_names(&locals, *name, NULL);

        table_kill(&locals);

        return rtn;
    }
    else if (TYPE_IS_CHARVEC((*items)->type))
    {
        table_s locals;
        table_init(&locals);

        VEC_FOREACH(&(*items)->val.v, char *, c)
        {
            val_s *item, *result;

            item = val_create(TYPE_STR);
            vec_app(&(item->val.v), c);

            eval_set_str_names(&locals, *name, item);
            val_ref(*code);
            result = eval_deferred(*code, f, globals, &locals);

            if (result)
            {
                val_app(rtn, result);
                val_deref(result);
            }

            val_deref(item);
        }

        eval_set_str_names(&locals, *name, NULL);

        table_kill(&locals);

        return rtn;
    }

    return NULL;
}

static val_s *builtin_default(val_s *call, frame_s *f, table_s *globals)
{
    val_s **name, **def, **rtn;
    vec_s  *chars;
    table_s *locals;

    if (call->type != TYPE_CALL)
        return NULL;

    if (vec_len(&(call->val.v)) != 3)
        return NULL;

    name = vec_get(&(call->val.v), 1);
    def  = vec_get(&(call->val.v), 2);


    if ((*name)->type != TYPE_STR)
        return NULL;

    chars = &((*name)->val.v);

    locals = &(f->local);

    rtn = table_get(locals, vec_get(chars, 0), vec_len(chars), NULL);
    if (rtn)
    {
        val_ref(*rtn);
        return *rtn;
    }

    rtn = table_get(globals, vec_get(chars, 0), vec_len(chars), NULL);
    if (rtn)
    {
        val_ref(*rtn);
        return *rtn;
    }

    val_ref(*def);
    return *def;
}

static val_s *builtin_global(val_s *call, frame_s *f, table_s *globals)
{
    val_s **name, **val;

    if (call->type != TYPE_CALL)
        return NULL;

    if (vec_len(&(call->val.v)) != 3)
        return NULL;

    name = vec_get(&(call->val.v), 1);
    val  = vec_get(&(call->val.v), 2);

    eval_set_str_names(globals, *name, *val);

    return NULL;
}

static val_s *builtin_set(val_s *call, frame_s *f, table_s *globals)
{
    val_s **name, **val;

    if (call->type != TYPE_CALL)
        return NULL;

    if (vec_len(&(call->val.v)) != 3)
        return NULL;

    name = vec_get(&(call->val.v), 1);
    val  = vec_get(&(call->val.v), 2);

    eval_set_str_names(&(f->local), *name, *val);

    return NULL;
}

static inline val_s *builtin_apply_to_args(
    val_s *call, frame_s *f,
    table_s *globals, int (*funct)(val_s *, val_s *))
{
    val_s *rtn, **inplace;
    size_t nargs, ind;
    nargs = vec_len(&(call->val.v)) - 1;

    if (call->type != TYPE_CALL)
        return NULL;

    if (nargs == 0)
        return NULL;

    inplace = vec_get(&(call->val.v), 1);
    *inplace = val_editable(*inplace);
    rtn = *inplace;
    val_ref(rtn);

    for (ind = 2; ind <= nargs; ++ind)
    {
        val_s **oth;
        oth = vec_get(&(call->val.v), ind);
        funct(rtn, *oth);
    }

    return rtn;
}

static val_s *builtin_plus(val_s *call, frame_s *f, table_s *globals)
{
    return builtin_apply_to_args(call, f, globals, val_incr);
}

static val_s *builtin_minus(val_s *call, frame_s *f, table_s *globals)
{
    return builtin_apply_to_args(call, f, globals, val_decr);
}

static val_s *builtin_multiply(val_s *call, frame_s *f, table_s *globals)
{
    return builtin_apply_to_args(call, f, globals, val_mult);
}

static val_s *builtin_print(val_s *call, frame_s *f, table_s *globals)
{
    size_t nargs, ind;
    nargs = vec_len(&(call->val.v)) - 1;

    for (ind = 1; ind <= nargs; ++ind)
    {
        val_s **v;
        v = vec_get(&(call->val.v), ind);

        if ((*v)->type == TYPE_STR)
        {
            vec_s *str;
            str = &((*v)->val.v);
            fwrite(vec_get(str, 0), 1, vec_len(str), stdout);
        }
        else
        {
            print_val_to_file(*v, stdout);
        }
    }

    return NULL;
}

static val_s *builtin_call(val_s *call, frame_s *f, table_s *globals)
{
    val_s *virtualcall, **args, **funct;
    size_t nargs;
    nargs = vec_len(&(call->val.v)) - 1;

    if (nargs != 2)
        return NULL;

    funct = vec_get(&(call->val.v), 1);
    args  = vec_get(&(call->val.v), 2);
    if (!TYPE_IS_VALVEC((*args)->type))
        return NULL;

    virtualcall = val_create(TYPE_CALL);
    val_app(virtualcall, *funct);
    VEC_FOREACH(&((*args)->val.v), val_s **, v)
    {
        val_app(virtualcall, *v);
    }

    return eval_callify(virtualcall, f, globals);
}
