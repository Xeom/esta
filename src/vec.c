#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "vec.h"

static int vec_resize_longer(vec_s *v);
static int vec_resize_shorter(vec_s *v);

void vec_init(vec_s *v, size_t width)
{
    memset(v, 0, sizeof(vec_s));
    v->width = width;
}

void vec_kill(vec_s *v)
{
    if (v->mem)
        free(v->mem);

    memset(v, 0, sizeof(vec_s));
}

static int vec_resize_longer(vec_s *v)
{
    if (v->used <= v->allocated)
        return 0;

    if (v->allocated == 0)
        v->allocated = 1;

    do
    {
        v->allocated <<= 1;
    } while (v->used > v->allocated);

    v->mem = realloc(v->mem, v->allocated);

    if (!(v->mem)) return -1;

    return 0;
}

static int vec_resize_shorter(vec_s *v)
{
    if (v->used >= v->allocated >> 2)
        return 0;

    do
    {
        v->allocated >>= 1;
    } while (v->used < v->allocated >> 2);

    v->mem = realloc(v->mem, v->allocated);

    if (!(v->mem)) return -1;

    return 0;
}

void *vec_ins(vec_s *v, size_t ind, size_t n, void *mem)
{
    size_t offset, numbytes, bytesafter;

    if (n == 0) return NULL;

    numbytes   = n   * v->width;
    offset     = ind * v->width;
    bytesafter = v->used - offset;

    if (offset > v->used) return NULL;

    v->used += numbytes;
    vec_resize_longer(v);

    if (bytesafter != 0)
        memmove(v->mem + offset + numbytes, v->mem + offset, bytesafter);

    if (mem)
        memcpy(v->mem + offset, mem, numbytes);
    else
        memset(v->mem + offset, 0, numbytes);

    return v->mem + offset;
}

int vec_del(vec_s *v, size_t ind, size_t n)
{
    size_t offset, numbytes, bytesafter;

    if (n == 0) return 0;

    numbytes   = n   * v->width;
    offset     = ind * v->width;
    bytesafter = v->used - offset - numbytes;

    if (offset > v->used || numbytes > v->used) return -1;

    if (bytesafter != 0)
        memmove(v->mem + offset, v->mem + offset + numbytes, bytesafter);

    v->used -= numbytes;
    vec_resize_shorter(v);

    return 0;
}

void *vec_get(vec_s *v, size_t ind)
{
    size_t offset;

    offset = ind * v->width;

    if (offset > v->used) return NULL;

    return v->mem + offset;
}

size_t vec_len(vec_s *v)
{
    return v->used / v->width;
}

void *vec_fmt(vec_s *v, char *fmt, ...)
{
    va_list args;
    void *rtn;
    int n;
    char c;
    size_t ind;

    va_start(args, fmt);

    ind = vec_len(v);
    n = vsnprintf(&c, 1, fmt, args);
    rtn = vec_add(v, n + 1, NULL);

    va_start(args, fmt);
    vsnprintf(vec_get(v, ind), n + 1, fmt, args);
    va_end(args);

    vec_del(v, vec_len(v) - 1, 1);

    return rtn;
}
