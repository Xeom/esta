#include <stdlib.h>

#include "parser.h"

static int parser_descend(parser_s **p, val_type_t type);
static int parser_ascend(parser_s **p);

void parser_init(parser_s *p, val_type_t type)
{
    p->val    = val_create(type);
    p->state  = PARSER_IDLE;
    p->parent = NULL;
}

void parser_kill(parser_s *p)
{
    parser_s *parent;
    val_deref(p->val);
    parent = p->parent;
    if (parent)
    {
        free(p);
        parser_kill(parent);
    }
}

static int parser_descend(parser_s **p, val_type_t type)
{
    int err;
    val_s *new;
    parser_s *newparser;

    new = val_create(type);

    err = val_app((*p)->val, new);

    val_deref(new);

    if (err) return -1;

    newparser = malloc(sizeof(parser_s));

    newparser->state  = PARSER_IDLE;
    newparser->val    = new;
    newparser->parent = *p;

    *p = newparser;

    return 0;
}

static int parser_ascend(parser_s **p)
{
    parser_s *newparser;

    newparser = (*p)->parent;

    if (newparser == NULL)
        return 0;

    free(*p);

    *p = newparser;

    return 0;
}

int parser_str(parser_s **p, char *str)
{
    for (; *str; ++str)
    {
        if (parser_chr(p, (int)*str) == -1)
            return -1;
    }

    return parser_chr(p, -1);
}

int parser_chr(parser_s **p, int c)
{
    int max;
    max = 50;
    while (max--)
    {
        switch(parser_process(p, c))
        {
        case 1:  return 0;
        case 0:  break;
        case -1: return -1;
        }
    }
    return -1;
}

int parser_process(parser_s **p, int c)
{
    val_s *v;
    v = (*p)->val;

    switch ((*p)->state)
    {
    case PARSER_IDLE:
        if (parser_is_number(c))
        {
            parser_descend(p, TYPE_INT);
            (*p)->state = PARSER_NUMBER;
            (*p)->val->val.i = 0;
            return 0;
        }
        if (parser_is_namestart(c))
        {
            parser_descend(p, TYPE_NAME);
            (*p)->state = PARSER_NAME;
            return 0;
        }
        if (parser_is_whitespace(c))
        {
            return 1;
        }

        switch (c)
        {
        case '[':
            (*p)->state = PARSER_LIST;
            parser_descend(p, TYPE_LIST);
            return 1;
        case '(':
            (*p)->state = PARSER_CALL;
            parser_descend(p, TYPE_CALL);
            return 1;
        case '{':
            (*p)->state = PARSER_DEFERRED;
            parser_descend(p, TYPE_DEFERRED);
            return 1;
        case '\'':
            parser_descend(p, TYPE_STR);
            (*p)->state = PARSER_BABY_STRING;
            return 1;
        case '"':
            parser_descend(p, TYPE_STR);
            (*p)->state = PARSER_STRING;
            return 1;
        case ')':
        case ']':
        case '}':
            parser_ascend(p);
            return 0;
        }

        // Unexpected character
        return -1;

    case PARSER_NAME:
        if (parser_is_namemiddle(c))
        {
            vec_app(&(v->val.v), &c);
            return 1;
        }
        else
        {
            parser_ascend(p);
            return 0;
        }

    case PARSER_NUMBER:
        if (parser_is_number(c))
        {
            v->val.i *= 10;
            v->val.i += c - '0';
            return 1;
        }
        else if (c == 'x' && v->val.i == 0)
        {
            (*p)->state = PARSER_HEX;
            return 1;
        }
        else
        {
            parser_ascend(p);
            return 0;
        }

    case PARSER_HEX:
        if (parser_is_number(c))
        {
            v->val.i *= 16;
            v->val.i += c - '0';
            return 1;
        }
        else if (c >= 'a' && c <= 'f')
        {
            v->val.i *= 16;
            v->val.i += 0x0a + c - 'a';
            return 1;
        }
        else if (c >= 'A' && c <= 'F')
        {
            v->val.i *= 16;
            v->val.i += 0x0a + c - 'A';
            return 1;
        }
        else
        {
            parser_ascend(p);
            return 0;
        }

    case PARSER_BABY_STRING:
        if (parser_is_namemiddle(c))
        {
            vec_app(&(v->val.v), &c);
            return 1;
        }
        else
        {
            parser_ascend(p);
            return 0;
        }

    case PARSER_STRING:
        if (c == '\\')
        {
            (*p)->state = PARSER_STRING_ESCAPE;
        }
        else if (c == '"')
        {
            parser_ascend(p);
        }
        else
        {
            vec_app(&(v->val.v), &c);
        }
        return 1;

    case PARSER_STRING_ESCAPE:
        (*p)->state = PARSER_STRING;
        switch (c)
        {
        case 'x':
            (*p)->state = PARSER_STRING_ESCAPE_HEX1;
            return 1;
        case 'n':
            vec_str(&(v->val.v), "\n");
            return 1;
        case 'r':
            vec_str(&(v->val.v), "\r");
            return 1;
        case 'b':
            vec_str(&(v->val.v), "\b");
            return 1;
        case '\\':
            vec_str(&(v->val.v), "\\");
            return 1;
        default:
            return -1;
        }

    case PARSER_STRING_ESCAPE_HEX1:
        (*p)->state = PARSER_STRING_ESCAPE_HEX2;
        if (c >= '0' && c <= '9')
            c -= '0';
        else if (c >= 'a' && c <= 'f')
            c -= 'a';
        else if (c >= 'A' && c <= 'F')
            c -= 'A';
        else
            return -1;

        vec_app(&(v->val.v), &c);
        return 1;

    case PARSER_STRING_ESCAPE_HEX2:
        (*p)->state = PARSER_STRING;
        if (c >= '0' && c <= '9')
            c -= '0';
        else if (c >= 'a' && c <= 'f')
            c -= 'a';
        else if (c >= 'A' && c <= 'F')
            c -= 'A';
        else
            return 0;

        *(char *)vec_end(&(v->val.v)) <<= 4;
        *(char *)vec_end(&(v->val.v))  |= c;

        return 1;

    case PARSER_LIST:
        (*p)->state = PARSER_IDLE;
        if (c != ']')
        {
            return -1;
        }
        return 1;

    case PARSER_CALL:
        (*p)->state = PARSER_IDLE;
        if (c != ')')
        {
            return -1;
        }
        return 1;

    case PARSER_DEFERRED:
        (*p)->state = PARSER_IDLE;
        if (c != '}')
        {
            return -1;
        }
        return 1;

    default:
        (*p)->state = PARSER_IDLE;
        return 0;
    }

    return 1;
}

