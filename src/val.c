#include "val.h"
#include "vec.h"

#include <stdlib.h>

bool val_bool(val_s *val)
{
    if (val->type == TYPE_FLOAT)
        return val->val.f != 0;

    if (val->type == TYPE_INT)
        return val->val.i != 0;

    if (TYPE_IS_VEC(val->type))
        return vec_len(&(val->val.v)) != 0;

    return false;
}

bool val_eq(val_s *a, val_s *b)
{
    vec_s *avec, *bvec;

    if (a == b) return true;

    if ((!a && b) || (!b && a))
        return false;

    if (a->type != b->type)
        return false;

    avec = &(a->val.v);
    bvec = &(b->val.v);

    if (TYPE_IS_VEC(a->type) && vec_len(avec) != vec_len(bvec))
        return false;

    if (TYPE_IS_VALVEC(a->type))
    {
        VEC_FOREACH(avec, val_s **, aitem)
        {
            val_s **bitem;
            bitem = vec_get(bvec, _ind);

            if (!val_eq(*aitem, *bitem))
                return false;
        }

        return true;
    }
    else if (TYPE_IS_CHARVEC(a->type))
    {
        if (memcmp(vec_get(avec, 0), vec_get(bvec, 0), vec_len(avec)) != 0)
            return false;

        return true;
    }
    else switch (a->type)
    {
    case TYPE_INT:   return a->val.i == b->val.i;
    case TYPE_FLOAT: return a->val.f == b->val.f;
    }

    return false;
}

void val_deref(val_s *val)
{
    if (val == NULL) return;

    val->refs -= 1;

    if (val->refs <= 0)
    {
        if (TYPE_IS_VALVEC(val->type))
        {
            VEC_FOREACH(&(val->val.v), val_s **, v)
            {
                val_deref(*v);
            }
        }

        if (TYPE_IS_VEC(val->type))
            vec_kill(&(val->val.v));

//        printf("Free'd value %p\n", val);
        free(val);
    }
}

void val_ref(val_s *val)
{
    val->refs += 1;
}

val_s *val_create(val_type_t type)
{
    val_s *rtn;
    rtn = malloc(sizeof(val_s));

    rtn->type = type;
    rtn->refs = 1;

//    printf("Made value %p\n", rtn);

    if (TYPE_IS_CHARVEC(type))
        vec_init(&(rtn->val.v), sizeof(char));

    if (TYPE_IS_VALVEC(type))
        vec_init(&(rtn->val.v), sizeof(val_s *));

    return rtn;
}

val_s *val_editable(val_s *val)
{
    val_s *rtn;

    if (val->refs == 1) return val;

    rtn = val_create(val->type);

    if (TYPE_IS_VEC(val->type))
        vec_cpy(&(rtn->val.v), &(val->val.v));
    else
        memcpy(&(rtn->val), &(val->val), sizeof(rtn->val));

    if (TYPE_IS_VALVEC(val->type))
    {
        VEC_FOREACH(&(rtn->val.v), val_s **, v)
        {
            val_ref(*v);
        }
    }

    val_deref(val);

    return rtn;
}

int val_ins(val_s *val, size_t ind, size_t n, val_s **new)
{
    size_t i;

    if (!TYPE_IS_VALVEC(val->type))
        return -1;

    if (vec_ins(&(val->val.v), ind, n, new) == NULL)
        return -1;

    for (i = 0; i < n; ++i)
        val_ref(new[i]);

    return 0;
}

int val_app(val_s *val, val_s *new)
{
    if (!TYPE_IS_VALVEC(val->type))
        return -1;

    if (vec_app(&(val->val.v), &new) == NULL)
        return -1;

    val_ref(new);

    return 0;
}

size_t val_len(val_s *val)
{
    if (!TYPE_IS_VALVEC(val->type))
        return -1;

    return vec_len(&(val->val.v));
}

int val_incr(val_s *a, val_s *b)
{
    if (a->type != b->type)
        return -1;

    if (TYPE_IS_VALVEC(a->type))
    {
        VEC_FOREACH(&(b->val.v), val_s **, item)
        {
            val_app(a, *item);
        }
        return 0;
    }

    if (TYPE_IS_CHARVEC(a->type))
    {
        vec_cpy(&(a->val.v), &(b->val.v));
        return 0;
    }

    switch (a->type)
    {
    case TYPE_INT:
        a->val.i += b->val.i;
        return 0;
    case TYPE_FLOAT:
        a->val.f += b->val.f;
        return 0;
    default:
        return -1;
    }
}

int val_decr(val_s *a, val_s *b)
{
    if (a->type != b->type)
        return -1;

    switch (a->type)
    {
    case TYPE_INT:
        a->val.i -= b->val.i;
        return 0;
    case TYPE_FLOAT:
        a->val.f -= b->val.f;
        return 0;
    default:
        return -1;
    }
}

int val_mult(val_s *a, val_s *b)
{
    if (TYPE_IS_VEC(a->type) && b->type == TYPE_INT)
    {
        int rep, ind, len;
        vec_s *items;
        items = &(a->val.v);
        len = vec_len(items);

        for (rep = 0; rep < b->val.i - 1; ++rep)
        {
            if (TYPE_IS_VALVEC(a->type))
            {
                for (ind = 0; ind < len; ++ind)
                {
                    val_s *v;
                    v = *(val_s **)vec_get(items, ind);
                    val_app(a, v);
                }
            }
            else
            {
                char *start;
                vec_add(items, len, NULL);
                start = vec_get(items, vec_len(items) - len);
                memcpy(start, vec_get(items, 0), len);
            }
        }

        return 0;
    }

    if (a->type != b->type)
        return -1;

    switch (a->type)
    {
    case TYPE_INT:
        a->val.i *= b->val.i;
        return 0;
    case TYPE_FLOAT:
        a->val.f *= b->val.f;
        return 0;
    default:
        return -1;
    }    
}
