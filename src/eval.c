#include <stdlib.h>
#include "eval.h"

void eval_descend(frame_s **f, val_s *var)
{
    frame_s *new;

    new = malloc(sizeof(frame_s));
    new->parent = *f;
    *f = new;

    val_ref(var);

    table_init(&(new->local));
    new->var   = var;
    new->ind   = 0;
    new->rtn   = NULL;
}

val_s *eval_ascend(frame_s **f)
{
    frame_s *parent;
    val_s *rtn;

    TABLE_FOREACH(&((*f)->local), pair)
    {
        val_s **v;
        v = table_chain_val(pair);
        val_deref(*v);
    }

    val_deref((*f)->var);
    table_kill(&((*f)->local));

    parent = (*f)->parent;
    rtn    = (*f)->rtn;

    free(*f);
    *f = parent;

    return rtn;
}

val_s *eval_varify(val_s *v, table_s *local, table_s *global)
{
    val_s **rtn;

    if (v->type != TYPE_NAME)
        return v;

    if (local)
    {
        rtn = table_get(local, vec_get(&(v->val.v), 0), vec_len(&(v->val.v)), NULL);
        if (rtn)
        {
            val_deref(v);
            val_ref(*rtn);
            return *rtn;
        }
    }

    if (global)
    {
        rtn = table_get(global, vec_get(&(v->val.v), 0), vec_len(&(v->val.v)), NULL);
        if (rtn)
        {
            val_deref(v);
            val_ref(*rtn);
            return *rtn;
        }
    }

    /* Error */
    return NULL;
}

val_s *eval_defaultify(val_s *v, table_s *local)
{
    val_s *rtn, *name, *funct, *def;

    if (v->type != TYPE_NAME)
        return v;

    val_ref(v);
    def = eval_varify(v, local, NULL);

    if (!def)
    {
        val_deref(v);
        return v;
    }

    rtn   = val_create(TYPE_CALL);
    funct = val_create(TYPE_NAME);
    name  = val_create(TYPE_STR);

    vec_cpy(&(name->val.v),  &(v->val.v));
    vec_str(&(funct->val.v), "default");

    val_deref(v);

    val_app(rtn, funct);
    val_app(rtn, name);
    val_app(rtn, def);

    val_deref(def);
    val_deref(name);
    val_deref(funct);

    return rtn;
}

val_s *eval_r_varify(val_s *v, table_s *local, table_s *global)
{
    if (!TYPE_IS_VALVEC(v->type))
    {
        val_s *rtn;
        rtn = eval_varify(v, local, global);

        if (!rtn)
            return v;

        return rtn;
    }

    if (v->type == TYPE_DEFERRED)
    {
        return eval_r_defaultify(v, local);
    }

    v = val_editable(v);

    VEC_FOREACH(&(v->val.v), val_s **, item)
    {
        *item = eval_r_varify(*item, local, global);
    }

    return v;
}

val_s *eval_r_defaultify(val_s *v, table_s *local)
{
    if (!TYPE_IS_VALVEC(v->type))
    {
        return eval_defaultify(v, local);
    }

    v = val_editable(v);

    VEC_FOREACH(&(v->val.v), val_s **, item)
    {
        *item = eval_r_defaultify(*item, local);
    }

    return v;
}

val_s *eval_callify(val_s *v, frame_s *f, table_s *globals)
{
    val_s **funct;

    if (v->type != TYPE_CALL)
    {
        return v;
    }

    if (vec_len(&(v->val.v)) == 0)
    {
        val_deref(v);
        return NULL;
    }

    funct = vec_get(&(v->val.v), 0);

    if ((*funct)->type == TYPE_BUILTIN)
    {
        val_s *rtn;

        rtn = ((*funct)->val.builtin)(v, f, globals);
        val_deref(v);

        return rtn;
    }

    return NULL;
}

val_s *eval_r_callify(val_s *v, frame_s *f, table_s *globals)
{
    if (!TYPE_IS_VALVEC(v->type) || v->type == TYPE_DEFERRED)
        return v;

    v = val_editable(v);

    VEC_FOREACH(&(v->val.v), val_s **, item)
    {
        *item = eval_r_callify(*item, f, globals);
    }

    VEC_RFOREACH(&(v->val.v), val_s **, delitem)
    {
        if (*delitem == NULL)
            vec_del(&(v->val.v), 1, _ind);
    }

    if (v->type == TYPE_CALL)
        return eval_callify(v, f, globals);

    return v;
}

val_s *eval_line(val_s *v, frame_s *f, table_s *globals)
{
    v = eval_r_varify(v, &(f->local), globals);
    v = eval_r_callify(v, f, globals);
    return v;
}

val_s *eval_deferred(val_s *v, frame_s *f, table_s *globals, table_s *locals)
{
    val_s *rtn;

    if (v->type != TYPE_DEFERRED)
        return v;

    eval_descend(&f, v);
    v = val_editable(v);

    if (locals)
    {
        TABLE_FOREACH(locals, pair)
        {
            table_set(&(f->local),
                table_chain_key(pair), pair->keylen,
                table_chain_val(pair), pair->vallen
            );
            val_ref(*(val_s **)table_chain_val(pair));
        }
    }

    rtn = NULL;
    VEC_FOREACH(&(v->val.v), val_s **, line)
    {
        if (rtn)
            val_deref(rtn);

        rtn = eval_line(*line, f, globals);
        *line = NULL;
    }

    eval_ascend(&f);
    val_deref(v);

    return rtn;
}

int eval_set_str_names(table_s *scope, val_s *name, val_s *v)
{
    val_s **prev;
    vec_s *str;

    if (name->type != TYPE_STR)
        return -1;

    str = &(name->val.v);
    prev = table_get(scope, vec_get(str, 0), vec_len(str), NULL);

    if (v)
        val_ref(v);

    if (prev)
        val_deref(*prev);

    if (v)
        table_set(scope, vec_get(str, 0), vec_len(str), &v, sizeof(val_s *));
    else
        table_del(scope, vec_get(str, 0), vec_len(str));

    return 0;
}
