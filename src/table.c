#include <stdint.h>
#include <stdlib.h>
#include "table.h"

static void table_resize_smaller(table_s *t);
static void table_resize_bigger(table_s *t);
static void table_relocate_chain(table_s *t, table_chain_s **chain);
static table_chain_s **table_find_chain(table_s *t, char *key, size_t keylen);
static int table_chain_del(table_chain_s **chain, char *key, size_t keylen);
static void table_chain_add(table_chain_s **chain, table_chain_s *new);
static int table_chain_keycmp(table_chain_s *chain, char *key, size_t keylen);
static table_chain_s *table_chain_find(table_chain_s *chain, char *key, size_t keylen);
static table_chain_s *table_chain_create(char *key, size_t keylen, char *val, size_t vallen);
static void table_chain_kill(table_chain_s *chain);
uint64_t table_hash(char *mem, size_t n);

uint64_t table_hash(char *mem, size_t n)
{
    uint64_t hash;
    uint32_t i;

    i    = 0;
    hash = 0xfc64c9bd39ea2141;

    while (n > 0)
    {
        double   recp, den;
        void    *fptr;

        if (n >= 4)
        {
            i    = *(uint32_t *)mem;

            mem += 4;
            n   -= 4;
        }
        else
        {
            i = 0;
            while (n)
            {
                 i <<= 8;
                 i |= mem[--n];
            }
        }

        den  = ((double)i * 2.0) - 1;

        recp = 1 / (double)den;
        fptr = &recp;

        hash ^= ((1l << 52) - 1) & *(long long *)fptr;
        hash ^= (hash << 26);
        hash ^= i;
    }

    return hash;
}

void *table_chain_key(table_chain_s *chain)
{
    return chain->data;
}

void *table_chain_val(table_chain_s *chain)
{
    return chain->data + chain->keylen;
}

static table_chain_s *table_chain_create(char *key, size_t keylen, char *val, size_t vallen)
{
    size_t size;
    table_chain_s *rtn;

    size = sizeof(table_chain_s) + keylen + vallen;
    rtn  = malloc(size);

    rtn->next   = NULL;
    rtn->keylen = keylen;
    rtn->vallen = vallen;

    memcpy(table_chain_key(rtn), key, keylen);
    memcpy(table_chain_val(rtn), val, vallen);

    return rtn;
}

static table_chain_s *table_chain_find(table_chain_s *chain, char *key, size_t keylen)
{
    while (chain)
    {
        if (table_chain_keycmp(chain, key, keylen) == 0)
            return chain;

        chain = chain->next;
    }

    return NULL;
}

static int table_chain_keycmp(table_chain_s *chain, char *key, size_t keylen)
{
    if (keylen != chain->keylen)
        return -1;

    return memcmp(table_chain_key(chain), key, keylen);
}

static void table_chain_add(table_chain_s **chain, table_chain_s *new)
{
    if (*chain == NULL)
    {
        *chain = new;
    }
    else
    {
        new->next      = (*chain)->next;
        (*chain)->next = new;
    }
}

static int table_chain_del(table_chain_s **chain, char *key, size_t keylen)
{
    while (*chain)
    {
        if (table_chain_keycmp(*chain, key, keylen) == 0)
        {
            table_chain_s *tofree;
            tofree = *chain;
            *chain = (*chain)->next;
            free(tofree);
            return 0;
        }
        chain = &((*chain)->next);
    }

    return -1;
}

static void table_chain_kill(table_chain_s *chain)
{
    if (!chain)
        return;

    if (chain->next)
        table_chain_kill(chain->next);

    free(chain);
}

void table_init(table_s *t)
{
    t->nitems  = 0;
    t->nchains = TABLE_MIN_SIZE;
    vec_init(&(t->chains), sizeof(table_chain_s **));
    vec_add(&(t->chains), TABLE_MIN_SIZE, NULL);
}

void table_kill(table_s *t)
{
    VEC_FOREACH(&(t->chains), table_chain_s **, chain)
    {
        table_chain_kill(*chain);
    }

    vec_kill(&(t->chains));
}

static table_chain_s **table_find_chain(table_s *t, char *key, size_t keylen)
{
    uint64_t hash;
    hash  = table_hash(key, keylen);
    hash %= t->nchains;

    return vec_get(&(t->chains), hash);
}

static void table_relocate_chain(table_s *t, table_chain_s **chain)
{
    table_chain_s **iter, **new;
    iter = chain;
    while (*iter)
    {
        new = table_find_chain(t, table_chain_key(*iter), (*iter)->keylen);

        if (new != chain)
        {
            table_chain_add(new, *iter);
            *iter = (*iter)->next;
        }
        else
        {
            iter = &((*iter)->next);
        }
    }
}

static void table_resize_bigger(table_s *t)
{
    size_t len, ind;
    len = vec_len(&(t->chains));

    vec_add(&(t->chains), len, NULL);
    t->nchains <<= 1;

    for (ind = 0; ind < len; ++ind)
        table_relocate_chain(t, vec_get(&(t->chains), ind));
}

static void table_resize_smaller(table_s *t)
{
    size_t len, ind;
    len = vec_len(&(t->chains));
    t->nchains >>= 1;

    for (ind = len >> 1; ind < len; ++ind)
        table_relocate_chain(t, vec_get(&(t->chains), ind));

    vec_del(&(t->chains), len >> 1, len - (len >> 1));
}

void *table_get(table_s *t, void *key, size_t keylen, size_t *vallen)
{
    table_chain_s *chain;
    chain = *table_find_chain(t, key, keylen);
    chain = table_chain_find(chain, key, keylen);

    if (!chain) return NULL;

    if (vallen)
        *vallen = chain->vallen;

    return table_chain_val(chain);
}

void table_set(table_s *t, void *key, size_t keylen, void *val, size_t vallen)
{
    table_chain_s **chain, *new;
    chain = table_find_chain(t, key, keylen);
    if (table_chain_del(chain, key, keylen) != 0)
    {
        size_t len;
        len = vec_len(&(t->chains));

        t->nitems += 1;

        if (t->nitems > len >> 1)
            table_resize_bigger(t);

        chain = table_find_chain(t, key, keylen);
    }

    new = table_chain_create(key, keylen, val, vallen);
    table_chain_add(chain, new);
}

int table_del(table_s *t, void *key, size_t keylen)
{
    table_chain_s **chain;
    chain = table_find_chain(t, key, keylen);

    if (table_chain_del(chain, key, keylen) == 0)
    {
        size_t len;
        len = vec_len(&(t->chains));

        t->nitems -= 1;

        if (t->nitems < len >> 3 && len > TABLE_MIN_SIZE)
            table_resize_smaller(t);

        return 0;
    }
    else
    {
        return -1;
    }
}

table_chain_s *table_next(table_s *t, size_t *ind, table_chain_s *chain)
{
    if (!chain)
        *ind = 0;
    else
        chain = chain->next;

    while (!chain)
    {
        if (*ind == t->nchains)
        {
            *ind   = 0;
            return NULL;
        }

        chain = *(table_chain_s **)vec_get(&(t->chains), *ind);
        *ind += 1;
    }

    return chain;
}
