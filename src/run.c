#include "run.h"

val_s *parse_line(char *str)
{
    parser_s  parser;
    parser_s *p;
    val_s *rtn;

    p = &parser;
    parser_init(p, TYPE_DEFERRED);

    parser_str(&p, str);
    rtn = parser.val;
    val_ref(rtn);

    parser_kill(p);

    return rtn;
}

val_s *parse_file(FILE *f)
{
    parser_s  parser;
    parser_s *p;
    val_s *rtn;

    p = &parser;
    parser_init(p, TYPE_DEFERRED);

    while (1)
    {
        int c;
        c = fgetc(f);
        if (c == EOF)
            break;

        parser_chr(&p, c);
    }

    rtn = parser.val;
    val_ref(rtn);

    parser_kill(p);

    return rtn;
}

void run_file(FILE *f, table_s *globals)
{
    val_s *val;
    frame_s *frame = NULL;
    val = parse_file(f);
    val = eval_deferred(val, frame, globals, NULL);
    val_deref(val);
}

void run_line(char *str, table_s *globals)
{
    val_s *val;
    frame_s *frame = NULL;
    val = parse_line(str);
    val = eval_deferred(val, frame, globals, NULL);
    print_val_to_file(val, stdout);
    val_deref(val);
}
