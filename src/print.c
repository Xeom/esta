#include "parser.h"
#include "vec.h"
#include "print.h"

void print_valvec(val_s *v, vec_s *dst)
{
    switch (v->type)
    {
    case TYPE_DEFERRED: vec_str(dst, "{"); break;
    case TYPE_LIST:     vec_str(dst, "["); break;
    case TYPE_CALL:     vec_str(dst, "("); break;
    default:            vec_fmt(dst, "[<?%d>", v->type); break;
    }

    VEC_FOREACH(&(v->val.v), val_s **, val)
    {
        vec_str(dst, " ");
        print_val(*val, dst);
    }
    vec_str(dst, " ");

    switch (v->type)
    {
    case TYPE_DEFERRED: vec_str(dst, "}"); break;
    case TYPE_LIST:     vec_str(dst, "]"); break;
    case TYPE_CALL:     vec_str(dst, ")"); break;
    default:            vec_fmt(dst, "]<?%d>", v->type); break;
    }
}

void print_stringval(val_s *v, vec_s *dst)
{
    int hasspecial = 0;
    VEC_FOREACH(&(v->val.v), char *, c)
    {
        if (!parser_is_namemiddle(*c))
        {
            hasspecial = 1;
            break;
        }
    }

    if (!hasspecial)
    {
        vec_str(dst, "'");
        vec_cpy(dst, &(v->val.v));
    }
    else
    {
        vec_str(dst, "\"");
        VEC_FOREACH(&(v->val.v), unsigned char *, c)
        {
            if (*c < 0x20 || *c > 0x7e)
                vec_fmt(dst, "\\x%02x", *c);
            else if (*c == '\\')
                vec_str(dst, "\\\\");
            else
                vec_app(dst, (void *)c);
        }
        vec_str(dst, "\"");
    }
}

int print_val(val_s *v, vec_s *dst)
{
    if (!v)
    {
        vec_str(dst, "(null)");
        return 0;
    }

    if (TYPE_IS_VALVEC(v->type))
        print_valvec(v, dst);
    else switch (v->type)
    {
    case TYPE_INT:   vec_fmt(dst, "%d", v->val.i); break;
    case TYPE_FLOAT: vec_fmt(dst, "%f", v->val.f); break;
    case TYPE_STR:   print_stringval(v, dst);      break;
    case TYPE_NAME:  vec_cpy(dst, &(v->val.v));    break;
    case TYPE_BUILTIN: vec_fmt(dst, "(builtin 0x%016lx)", v->val.builtin); break;
    }

    return 0;
}

void print_val_to_file(val_s *v, FILE *f)
{
    vec_s result;

    if (!v)
    {
        fputc('\n', f);
        return;
    }

    vec_init(&result, sizeof(char));
    print_val(v, &result);
    vec_str(&result, "\n");
    fwrite(vec_get(&result, 0), 1, vec_len(&result), f);

    vec_kill(&result);
}
