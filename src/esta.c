#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <getopt.h>

#include "run.h"
#include "table.h"
#include "print.h"
#include "parser.h"
#include "eval.h"

static int interrupted = 0;

static void sighandle(int sig)
{
    puts("Interrupted ... ");
    interrupted = 1;
}

static void setup_signals(void)
{
    struct sigaction act;
    act.sa_handler = sighandle;
    sigemptyset(&(act.sa_mask));
    act.sa_flags = 0;

    sigaction(SIGINT, &act, NULL);
}

static void repl(table_s *globals)
{
    vec_s line;
    vec_init(&line, sizeof(char));
    while (1)
    {
        unsigned char c = '\0';

        vec_del(&line, 0, vec_len(&line));
        printf(" >>> ");
        fflush(stdout);

        while (1)
        {
            int cint;
            cint = fgetc(stdin);

            if (cint == EOF)
            {
                interrupted = 1;
                break;
            }

            c = (unsigned char)cint;

            if (c == '\n')
                break;

            vec_app(&line, &c);
        }

        vec_app(&line, NULL);

        if (interrupted) break;

        run_line(vec_get(&line, 0), globals);
    }
    vec_kill(&line);
}

struct config
{
    vec_s precode;
    vec_s files;
    bool  version;
    bool  repl;
};

static int parse_options(struct config *conf, int argc, char *argv[])
{
    static struct option opts[] =
    {
        { "run",     required_argument, 0, 'r' },
        { "file",    required_argument, 0, 'f' },
        { "version", no_argument,       0, 'v' },
        { "repl",    no_argument,       0, 'i' }
    };
    static char *optstr = "ivr:f:";
    int opt;

    memset(conf, 0, sizeof(struct config));
    vec_init(&(conf->precode), sizeof(char));
    vec_init(&(conf->files),   sizeof(FILE *));

    while ((opt = getopt_long(argc, argv, optstr, opts, NULL)) != -1)
    {
        FILE *f;
        switch (opt)
        {
        case 'r':
            vec_str(&(conf->precode), optarg);
            break;
        case 'f':
            f = fopen(optarg, "r");
            if (!f)
            {
                printf("Could not open file '%s': %s\n", optarg, strerror(errno));
                return -1;
            }
            vec_app(&(conf->files), &f);
            break;
        case 'v':
            conf->version = true;
            break;
        case 'i':
            conf->repl = true;
            break;
        case '?':
        default:
            return -1;
        }
    }

    return 0;
}

static int kill_config(struct config *conf)
{
    VEC_FOREACH(&(conf->files), FILE **, ptr)
    {
        fclose(*ptr);
    }

    vec_kill(&(conf->precode));
    vec_kill(&(conf->files));
}

static int run(struct config *conf)
{
    table_s globals;
    table_init(&globals);
    builtins_add(&globals);

    if (vec_len(&(conf->precode)))
    {
        vec_app(&(conf->precode), NULL);
        printf("Running precode: %s\n", (char *)vec_get(&(conf->precode), 0));
        run_line(vec_get(&(conf->precode), 0), &globals);
    }

    VEC_FOREACH(&(conf->files), FILE **, ptr)
    {
        FILE *f;
        f = *ptr;
        run_file(f, &globals);
    }

    if ((vec_len(&(conf->precode)) == 0 && vec_len(&(conf->files)) == 0) || conf->repl)
    {
        repl(&globals);
    }

    TABLE_FOREACH(&globals, pair)
    {
        val_s **v;
        v = table_chain_val(pair);
        val_deref(*v);
    }

    table_kill(&globals);

    return 0;
}

int main(int argc, char *argv[])
{
    int rtn;
    struct config conf;
    setup_signals();

    if (parse_options(&conf, argc, argv) != 0)
    {
        puts("Could not parse options");
        kill_config(&conf);
        return -1;
    }
    rtn = run(&conf);
    kill_config(&conf);

    return rtn;
}
